FROM openjdk:8-jdk-alpine
COPY build/libs/halyk-rest-api-1.0-SNAPSHOT.jar /halyk-rest-api-1.0-SNAPSHOT.jar
CMD ["java","-jar","/halyk-rest-api-1.0-SNAPSHOT.jar"]