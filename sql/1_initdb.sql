CREATE TABLE public."user" (
	"name" text NOT NULL,
	age int4 NOT NULL,
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY
);