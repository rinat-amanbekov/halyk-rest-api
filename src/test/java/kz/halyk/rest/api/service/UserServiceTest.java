package kz.halyk.rest.api.service;

import java.util.ArrayList;
import java.util.List;
import kz.halyk.rest.api.model.User;
import kz.halyk.rest.api.util.UserException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

  @Autowired
  UserService userService;

  @Test
  public void getUserByNameCheckNumbersInName() throws UserException {
    String name = "student2";
    Assert.assertNull(userService.getUserByName(name));
  }

  @Test
  public void getUserByNameCheck() {
    String name = "test test test test test";
    Assert.assertThrows("long name", UserException.class,
        () -> userService.getUserByName(name));
  }

  @Test
  public void getUserByNameCheckWithMock() throws UserException {
    String name = "test test test test test";
    //define mock
    UserService userServiceMock = Mockito.mock(UserService.class);

    //prepare action
    Mockito.when(userServiceMock.getUserByName(name)).thenThrow(UserException.class);

    //test
    Assert.assertThrows("long name", UserException.class,
        () -> userServiceMock.getUserByName(name));
  }

  @Test
  public void getUsers() throws UserException {
    //prepare mock response
    User expectedUser = new User();
    expectedUser.setName("student");
    expectedUser.setId(1);
    expectedUser.setAge(23);
    List<User> expectedUserList = new ArrayList<>();
    expectedUserList.add(expectedUser);
    UserService userServiceMock = Mockito.mock(UserService.class);

    //define action
    Mockito.when(userServiceMock.getUserByName("student")).thenReturn(expectedUserList);

    //test
    String name = "student";
    List<User> users =  userServiceMock.getUserByName(name);
    Assert.assertTrue(users.size() > 0);
    User user = users.get(0);
    Assert.assertEquals(user.getName(), name);
  }
}
