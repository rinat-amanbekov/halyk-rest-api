package kz.halyk.rest.api.service;

import java.util.List;
import kz.halyk.rest.api.model.User;
import kz.halyk.rest.api.repository.UserRepository;
import kz.halyk.rest.api.util.UserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for users functions.
 * */
@Service
public class UserService {

  @Autowired
  UserRepository userRepository;

  /**
   * Get all users.
   * */
  public List<User> getUsers() {
    return (List<User>) userRepository.findAll();
  }

  /**
   * Get user by name.
   *
   * @param name of user
   *
   * */
  public List<User> getUserByName(String name) throws UserException {
    //name handler: names with numbers not allowed
    if (name.matches(".*\\d.*")) {
      return null;
    }
    if (name.split(" ").length > 4) {
      throw new UserException("long name");
    }
    return userRepository.getUserByName(name);
  }

  /**
   * Get insert user.
   * */
  public User save(User user) {
    return userRepository.save(user);
  }

  public void clear(User user) {
    userRepository.delete(user);
  }
}
