package kz.halyk.rest.api.repository;

import java.util.List;
import kz.halyk.rest.api.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends CrudRepository<User, Long> {
  @Query(value = "SELECT * FROM public.\"user\" u WHERE u.\"name\" = :name",
          nativeQuery = true)
  List<User> getUserByName(@Param("name") String name);
}
